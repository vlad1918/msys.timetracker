package msys.timetracker;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import msys.timetracker.db.DBAdapter;
import msys.timetracker.Utils;
import msys.timetracker.layout.WorkdayView;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.os.Bundle;

public class DetailsActivity extends ActionBarActivity {
	
	private Date targetDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DBAdapter.init(this);
		setContentView(R.layout.activity_details);
		
		targetDate = (getIntent().getStringExtra("targetDate") != null) 
				   ? Date.valueOf(getIntent().getStringExtra("targetDate")) 
				   : new Date(System.currentTimeMillis());
		
		TextView textView = (TextView) findViewById(R.id.monthLabel);
		textView.setGravity(Gravity.CENTER);
		textView.setTextSize(20);
		textView.setText(Utils.MON_YEAR_DATE_FORMAT.format(targetDate));
		
		//Populate the days layout with a panel for each day of the month until today
		LinearLayout daysLayout = (LinearLayout) findViewById(R.id.daysLayout);	
		Calendar cal = new GregorianCalendar();
		String[] split =  targetDate.toString().split("-");
		Integer dayToday = Integer.valueOf(split[2]);
		Integer dayNumber;
		List<Workday> allWorkdays = new ArrayList<Workday>();
		for (dayNumber = 1; dayNumber <= dayToday; dayNumber++) {
			Date date = Date.valueOf(split[0] + "-" + split[1] + "-" + dayNumber);
			cal.setTime(date);
			//Only add panel for work days, not weekends
			if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY 
					&& ! Utils.ROMANIAN_BANK_HOLIDAYS.contains(split[1] + "-" + String.format("%02d", dayNumber)) ) {
				List<Workday> workdays = DBAdapter.getWorkdayByDate(date);
				allWorkdays.addAll(workdays);
				if (workdays == null || workdays.size() == 0) {
					daysLayout.addView(new WorkdayView(null, date, this, false, getApplicationContext()));
				} else if (workdays.size() == 1) {
					daysLayout.addView(new WorkdayView(workdays.get(0), date, this, false, getApplicationContext()));
				} else {			
					for (Workday workday : workdays) {
						daysLayout.addView(new WorkdayView(workday, date, this, true, getApplicationContext()));					
					}
				}
			}
		}
		
		Workmode workmode = Utils.getWorkmode(getApplicationContext());
		String workEffort = Utils.calculateWorkEffort(allWorkdays, workmode);		
		TextView timetracker = (TextView) findViewById(R.id.timetracker2);
		timetracker.setText(workEffort);
		if ( workEffort.startsWith("-")) {
			timetracker.setTextColor(getResources().getColor(R.color.red));
		} else {
			timetracker.setTextColor(getResources().getColor(R.color.green));
		}
		
		//On a separate thread scroll the view to bottom
		final ScrollView scrollview = ((ScrollView) findViewById(R.id.daysScrollView));
		scrollview.post(new Runnable() {
		    @Override
		    public void run() {
		        scrollview.fullScroll(ScrollView.FOCUS_DOWN);
		    }
		});
	}

	//Called when the PreviousMonthButton is invoked
	public void onPreviousMonth(View view) {
		refreshActivityWithMonth(-1);
	}
	
	//Called when the NextMonthButton is invoked
	public void onNextMonth(View view) {
		refreshActivityWithMonth(1);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		Intent main = new Intent(this, MainActivity.class);
		startActivity(main);	
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}
	
	//Called when the preferences icon is pressed 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    //Go to the settings activity
		Intent settings = new Intent(this, SettingsActivity.class);
		startActivity(settings);	
		return true;
	}
	
	/**
	 * refreshes the activity by adding or subtracting months
	 * @param nbMonth
	 */
	private void refreshActivityWithMonth(int nbMonth) {
		Intent details = new Intent(this, DetailsActivity.class);
		Calendar now = GregorianCalendar.getInstance();
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(targetDate);
		cal.add(Calendar.MONTH, nbMonth);
		//If this is not the current month set date to last day of month
		if (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH)
				&&	cal.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
			cal = now;
		} else {
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		//Don't display dates in the past that don't have any registered work days 
		if (cal.compareTo(now) < 0 
				&& DBAdapter.getWorkdays(String.valueOf(cal.get(Calendar.YEAR)), 
						String.format(Locale.GERMANY, "%02d", (cal.get(Calendar.MONTH) + 1))).size() == 0) {
			Toast.makeText(getApplicationContext(), 
					getApplicationContext().getText(R.string.not_available_toast), Toast.LENGTH_SHORT).show();			
		}
		//Information in the future cannot be displayed
		else if (cal.compareTo(now) > 0) {
			Toast.makeText(getApplicationContext(), 
					getApplicationContext().getText(R.string.not_available_toast), Toast.LENGTH_SHORT).show();			
		} else {				
			finish();
			details.putExtra("targetDate", new Date(cal.getTimeInMillis()).toString());
			startActivity(details);
		}
	}
}
