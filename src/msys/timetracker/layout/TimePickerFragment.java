package msys.timetracker.layout;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

import msys.timetracker.Workday;
import msys.timetracker.db.DBAdapter;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment implements
		TimePickerDialog.OnTimeSetListener {

	private Workday workday;
	private Date date;
	private Boolean isStart;
	
	public TimePickerFragment(Workday workday, Date date, Boolean isStart) {
		this.workday = workday;
		this.date = date;
		this.isStart = isStart;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		int hourOfDay = 0;
		int minute = 0;
		
		if (workday != null) {
			if (isStart && workday.getStartTime() != null) {
				String[] split = workday.getStartTime().toString().split(":");
				hourOfDay 	= Integer.valueOf(split[0]);
				minute 		= Integer.valueOf(split[1]);
			} else if (! isStart && workday.getEndTime() != null) {
				String[] split = workday.getEndTime().toString().split(":");
				hourOfDay 	= Integer.valueOf(split[0]);
				minute 		= Integer.valueOf(split[1]);
			}
		}
		
		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hourOfDay, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		view.clearFocus();
		if (view.isShown()) {
			Calendar cal = GregorianCalendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, view.getCurrentHour());
			cal.set(Calendar.MINUTE, view.getCurrentMinute());	
			cal.set(Calendar.SECOND, 0);
			Time time = new Time(cal.getTimeInMillis());
	
			if (workday != null) { //Needs an update
				if (isStart) {			
					DBAdapter.updateWorkday(workday.getId(), time, null);
				} else {
					DBAdapter.updateWorkday(workday.getId(), null, time);
				}
			} else { //Needs an insert
				Workday newWorkday = new Workday();
				newWorkday.setDay(date);
				if (isStart) {
					newWorkday.setStartTime(time); 
				} else {
					newWorkday.setEndTime(time); 
				}
				DBAdapter.addWorkday(newWorkday);
			}
			
			//Refresh the details activity
			dismiss();
			getActivity().finish();
			startActivity(getActivity().getIntent());
		}
	}
}