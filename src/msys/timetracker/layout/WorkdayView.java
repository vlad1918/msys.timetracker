package msys.timetracker.layout;

import java.sql.Date;

import msys.timetracker.R;
import msys.timetracker.Utils;
import msys.timetracker.Workday;
import msys.timetracker.db.DBAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class WorkdayView extends LinearLayout {

	private FragmentActivity activity;
	private Workday workday;
	private Date date;
	private static final Integer TEXT_SIZE = 20; 
	
	/**
	 * Creates a panel for a work day. If work day is null than the panel will be created but with no time added to it 
	 * @param workday - can be NULL
	 * @param date
	 * @param activity
	 * @param isInterval
	 * @param context
	 */
	public WorkdayView(Workday workday, Date date, FragmentActivity activity, boolean isInterval, Context context) {
		super(context);
		
		this.setGravity(Gravity.CENTER);
		this.activity = activity;
		this.workday = workday;
		this.date = date;

		//Add the day number
		TextView dayNumberView = new TextView(context);
		dayNumberView.setText(Utils.DAY_NUMBER_DATE_FORMAT.format(date));
		dayNumberView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
		dayNumberView.setTextSize(TEXT_SIZE);
		dayNumberView.setGravity(Gravity.CENTER);
		dayNumberView.setTextColor(getResources().getColor(R.color.black));
		this.addView(dayNumberView);
		
		//Add the day short name or delete button if this is an interval
		if (isInterval) {
			ImageView deleteIntervalView = new ImageView(context);
			deleteIntervalView.setImageResource(android.R.drawable.ic_delete);
			deleteIntervalView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
			deleteIntervalView.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					AlertDialog.Builder alert = new AlertDialog.Builder(getActivity().getWindow().getContext());
					alert.setMessage(R.string.alert_delete);
					alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int n) {
							DBAdapter.deleteEntry(getWorkday().getId());
							getActivity().finish();
							getActivity().startActivity(getActivity().getIntent());
						}
					});
					alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int n) {
							dialog.dismiss();
						}
					});
					alert.show();
				}
			});	
			this.addView(deleteIntervalView);
		} else {
			TextView dayShortNameView = new TextView(context);
			dayShortNameView.setText(Utils.DAY_SHORT_NAME_DATE_FORMAT.format(date));
			dayShortNameView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
			dayShortNameView.setTextSize(TEXT_SIZE);
			dayShortNameView.setGravity(Gravity.CENTER);
			dayShortNameView.setTextColor(getResources().getColor(R.color.black));
			this.addView(dayShortNameView);
		}
		
		//Add start time
		TextView startTimeView = new TextView(context);
		if (workday != null && workday.getStartTime() != null) {
			startTimeView.setText(Utils.HOUR_MIN_DATE_FORMAT.format(workday.getStartTime()));
		} else {
			startTimeView.setText(Utils.NO_TIME_AVAILABLE);
		}
		startTimeView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
		startTimeView.setTextSize(TEXT_SIZE);
		startTimeView.setGravity(Gravity.CENTER);
		startTimeView.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
			    DialogFragment newFragment = new TimePickerFragment(getWorkday(), getDate(), true);
			    newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
			}
		});		
		startTimeView.setTextColor(getResources().getColor(R.color.black));
		this.addView(startTimeView);
		
		//Add end time
		TextView endTimeView = new TextView(context);
		if (workday != null && workday.getEndTime() != null) {
			endTimeView.setText(Utils.HOUR_MIN_DATE_FORMAT.format(workday.getEndTime()));
		} else {
			endTimeView.setText(Utils.NO_TIME_AVAILABLE);
		}
		endTimeView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
		endTimeView.setTextSize(TEXT_SIZE);
		endTimeView.setGravity(Gravity.CENTER);
		endTimeView.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
			    DialogFragment newFragment = new TimePickerFragment(getWorkday(), getDate(), false);
			    newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
			}
		});		
		endTimeView.setTextColor(getResources().getColor(R.color.black));
		this.addView(endTimeView);
		
		//Add dif view
		TextView difView = new TextView(context);
		difView.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, .20f));
		difView.setTextSize(TEXT_SIZE);
		difView.setGravity(Gravity.CENTER);
		difView.setTextColor(getResources().getColor(R.color.black)); 
		difView.setText(Utils.NO_TIME_AVAILABLE);
		if (workday != null && 
				(! isInterval && workday.getStartTime() != null && workday.getEndTime() != null)) {
			long delta = (workday.getEndTime().getTime() - workday.getStartTime().getTime()) / (60*1000);
			long dif = Utils.calculateWorkDayMinutes(workday.getDay(), delta, Utils.getWorkmode(context));
			String sDif = Utils.convertMinutesToHM((int)dif);
			if (sDif.startsWith("-")) {
				difView.setTextColor(getResources().getColor(R.color.red));
			} else {
				difView.setTextColor(getResources().getColor(R.color.green));
			}
			difView.setText(sDif);
		}
		this.addView(difView);
	}
	
	public FragmentActivity getActivity() {
		return activity;
	}
	
	public Workday getWorkday() {
		return workday;
	}
	
	public Date getDate() {
		return date;
	}
}