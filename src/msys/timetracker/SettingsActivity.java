package msys.timetracker;

import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.RadioButton;
import android.content.Intent;
import android.os.Bundle;

public class SettingsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		//Get the radio buttons
		RadioButton workmodeDefault 	= (RadioButton) findViewById(R.id.workmodeDefault);
		RadioButton workmodePregnancy 	= (RadioButton) findViewById(R.id.workmodePregnancy);
		RadioButton workmodePregnancy2 	= (RadioButton) findViewById(R.id.workmodePregnancy2);
		RadioButton workmodeInternship 	= (RadioButton) findViewById(R.id.workmodeInternship);
		RadioButton workmodeInternship2 = (RadioButton) findViewById(R.id.workmodeInternship2);
		
		//Get the current work mode and check the appropriate radio button
		Workmode workmode = Utils.getWorkmode(getApplicationContext());
		switch (workmode) {
			case PREGNANCY:
				workmodePregnancy.setChecked(true);
				break;
	
			case PREGNANCY2:
				workmodePregnancy2.setChecked(true);
				break;			
				
			case INTERNSHIP:
				workmodeInternship.setChecked(true);
				break;
				
			case INTERNSHIP2:
				workmodeInternship2.setChecked(true);
				break;			
				
			case DEFAULT:
			default:
				workmodeDefault.setChecked(true);
				break;
		}
	}

	//Called when workmodeDefault radio button is invoked
	public void onWorkmodeDefault(View view) {
		Utils.setWorkmode(getApplicationContext(), Workmode.DEFAULT);
	}
	
	//Called when workmodePregnancy radio button is invoked
	public void onWorkmodePregnancy(View view) {
		Utils.setWorkmode(getApplicationContext(), Workmode.PREGNANCY);
	}

	//Called when workmodePregnancy2 radio button is invoked
	public void onWorkmodePregnancy2(View view) {
		Utils.setWorkmode(getApplicationContext(), Workmode.PREGNANCY2);
	}
	
	//Called when workmodeInternship radio button is invoked
	public void onWorkmodeInternship(View view) {
		Utils.setWorkmode(getApplicationContext(), Workmode.INTERNSHIP);
	}
	
	//Called when workmodeInternship2 radio button is invoked
	public void onWorkmodeInternship2(View view) {
		Utils.setWorkmode(getApplicationContext(), Workmode.INTERNSHIP2);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		Intent main = new Intent(this, MainActivity.class);
		startActivity(main);	
	}
}
