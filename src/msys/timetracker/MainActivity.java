package msys.timetracker;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import msys.timetracker.db.DBAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private boolean needsTimeInterval = false;
	private Workday latestWorkday;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 	 	DBAdapter.init(this);
		setContentView(R.layout.activity_main);
		
		//Get the work days from the beginning of the month
		Date today = new Date(System.currentTimeMillis());		
		String[] split =  today.toString().split("-");		
		List<Workday> workdays = DBAdapter.getWorkdays(split[0], split[1]);
		Workmode workmode = Utils.getWorkmode(getApplicationContext());
		String workEffort = Utils.calculateWorkEffort(workdays, workmode);	
		
		workdays = DBAdapter.getWorkdayByDate(today);
		if (workdays.size() > 0) {
			latestWorkday = workdays.get(workdays.size() - 1);
			//If end time is null turn the button green and make it a checkout button
			if (latestWorkday.getEndTime() == null) {
				Button checkInButton = (Button) findViewById(R.id.btnCheckIn);
				checkInButton.setBackgroundColor(getResources().getColor(R.color.green));
				checkInButton.setText(R.string.btn_check_out);
			} else {
				needsTimeInterval = true;				
			}
		}
		
		TextView timetracker = (TextView) findViewById(R.id.timetracker);
		timetracker.setText(workEffort);
		if (workEffort.startsWith("-")) {
			timetracker.setTextColor(getResources().getColor(R.color.red));
		} else {
			timetracker.setTextColor(getResources().getColor(R.color.green));
		}	
		
		//If these are the last 10 days of the month and the work effort is negative than add a warning  
		Calendar cal = GregorianCalendar.getInstance();
		if (workEffort.startsWith("-") && (cal.getActualMaximum(Calendar.DAY_OF_MONTH) - 10 < cal.get(Calendar.DAY_OF_MONTH))) {
			TextView alert = (TextView) findViewById(R.id.alert);
			alert.setText(R.string.alert_month_end);
		}
		
	}

	//Called when the checkIn button is invoked
	public void onCheckIn(View view) {
		
		if (needsTimeInterval) {
			AlertDialog.Builder alert = new AlertDialog.Builder(getWindow().getContext());
			alert.setMessage(R.string.alert_interval);
			alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int n) {
					doCheckIn();
				}
			});
			alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int n) {
					dialog.dismiss();
				}
			});
			alert.show();			
		} else {
			doCheckIn();
		}
	}
	
	//Called when the Details button is invoked
	public void onDetails(View view) {	
		Intent details = new Intent(this, DetailsActivity.class);
		startActivity(details);		
	}
	
	@Override
	public void onBackPressed() {
		finish();
		moveTaskToBack(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}
	
	//Called when the preferences icon is pressed 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    //Go to the settings activity
		Intent settings = new Intent(this, SettingsActivity.class);
		startActivity(settings);	
		return true;
	}
	
	/**
	 * Performs a check in for today
	 */
	private void doCheckIn() {
		//Check to see if this is a week end day and display a toast
		if (GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY 
				|| GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			Toast.makeText(getApplicationContext(), 
					getApplicationContext().getText(R.string.weekend_toast), Toast.LENGTH_SHORT).show();
		} else {
			Long currentMillis = System.currentTimeMillis();	
			Date day = new Date(currentMillis);
//			Get the time but without seconds 
			Calendar cal = Calendar.getInstance();
			cal.setTime(day);
			cal.clear(Calendar.SECOND);
//			Time time = new Time(currentMillis);
			Time time = new Time(cal.getTimeInMillis());
			
			//Get all work days in that day
			List<Workday> workdays = DBAdapter.getWorkdayByDate(day);
			Workday workday;
			
			//If there are no work days it means that this is the first check in for that day so a new entry needs to be added
			if (workdays.size() == 0) {
				workday = new Workday();
				workday.setDay(day);
				workday.setStartTime(time);
				DBAdapter.addWorkday(workday);	
			} else {
				//Get the latest work day entry
				workday = workdays.get(workdays.size() - 1);
				
				//If the end time is null then update the work day with a check out time
				if (workday.getEndTime() == null) {
					DBAdapter.updateWorkday(workday.getId(), null, time);
				} else { //Else the worker has made a work break so a new work day needs to be added altogether 
					workday = new Workday();
					workday.setDay(day);
					workday.setStartTime(time);
					DBAdapter.addWorkday(workday);
				}
			}
			
			//Create confirmation message as a toast
			Toast.makeText(getApplicationContext(), 
					getApplicationContext().getText(R.string.check_in_toast) + " " 
							+ Utils.HOUR_MIN_DATE_FORMAT.format(time), Toast.LENGTH_LONG).show();
			
			//Reload the activity
			finish();
			startActivity(getIntent());
		}		
	}
}
