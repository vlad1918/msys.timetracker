package msys.timetracker;

import java.sql.Date;
import java.sql.Time;

/**
 * Entity class for the work day table in the database
 * @author vlad.dima
 *
 */
public class Workday {

	private Long id;
	private Date day;
	private Time startTime;
	private Time endTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

}
