package msys.timetracker.db;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import msys.timetracker.Workday;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {

	/******* if debug is set true then it will show all Logcat message ***/
	public static final boolean DEBUG = false;

	/********** Log cat TAG ************/
	public static final String LOG_TAG = "DBAdapter";

	/************ Table Fields ************/
	public static final String TABLE_NAME = "workday";
	public static final String COLUMN_NAME_ID = "id";
	public static final String COLUMN_NAME_DATE = "day";
	public static final String COLUMN_NAME_YEAR_MONTH = "year_month";
	public static final String COLUMN_NAME_START_TIME = "start_time";
	public static final String COLUMN_NAME_END_TIME = "end_time";

	private static final String TYPE_TEXT = " TEXT";
	private static final String COMMA_SEP = ",";

	/************* Database Name ************/
	public static final String DATABASE_NAME = "msys.timetracker.db";

	/**** Database Version (Increase one if want to also upgrade your database) ****/
	public static final int DATABASE_VERSION = 1;// started at 1

	/** Create table syntax */
	public static final String SQL_CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME
			+ " ("  + COLUMN_NAME_ID + " INTEGER PRIMARY KEY" + COMMA_SEP
			+ COLUMN_NAME_DATE + TYPE_TEXT + COMMA_SEP 
			+ COLUMN_NAME_YEAR_MONTH + TYPE_TEXT + COMMA_SEP 
			+ COLUMN_NAME_START_TIME + TYPE_TEXT + COMMA_SEP 
			+ COLUMN_NAME_END_TIME + TYPE_TEXT + " )";

	public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

	/********* Used to open database in syncronized way *********/
	private static DataBaseHelper DBHelper = null;

	protected DBAdapter() {

	}

	/********** Initialize database *********/
	public static void init(Context context) {
		if (DBHelper == null) {
			if (DEBUG)
				Log.i("DBAdapter", context.toString());
			DBHelper = new DataBaseHelper(context);
		}
	}

	/********** Main Database creation INNER class ********/
	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			if (DEBUG)
				Log.i(LOG_TAG, "new create");
			try {
				db.execSQL(SQL_CREATE_TABLE);					        		        	
			} catch (Exception exception) {
				if (DEBUG)
					Log.i(LOG_TAG, "Exception onCreate() exception");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//			if (DEBUG)
//				Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
//						+ "to" + newVersion + "...");

			//TODO update
//			db.execSQL(SQL_DROP_TABLE);
//			onCreate(db);
		}

	} // Inner class closed

	/***** Open database for insert,update,delete in syncronized manner ****/
	private static synchronized SQLiteDatabase open() throws SQLException {
		return DBHelper.getWritableDatabase();
	}

	/************* Data functions *************/
	/**
	 * Persist a work day
	 * @param workday
	 */
	public static void addWorkday(Workday workday) {

		// Open database for Read / Write
		final SQLiteDatabase db = open();

		String day = workday.getDay().toString();
		String[] split = day.split("-");
		String startTime = (workday.getStartTime() != null) ? workday.getStartTime().toString() : null;
		String endTime = (workday.getEndTime() != null) ? workday.getEndTime().toString() : null;

		ContentValues cVal = new ContentValues();

		cVal.put(COLUMN_NAME_DATE, day);
		cVal.put(COLUMN_NAME_YEAR_MONTH, split[0] + split[1]);
		cVal.put(COLUMN_NAME_START_TIME, startTime);
		cVal.put(COLUMN_NAME_END_TIME, endTime);

		// Insert user values in database
		db.insert(TABLE_NAME, null, cVal);
		db.close(); // Closing database connection
	}

	/**
	 * Gets all the work days in the given month
	 * @param year - in YYYY format
	 * @param month - in MM format 
	 * @return
	 */
	public static List<Workday> getWorkdays(String year, String month) {

		List<Workday> workDaysList = new ArrayList<Workday>();

		// Select All Query
		String selectQuery = "SELECT " + COLUMN_NAME_ID + ", " + COLUMN_NAME_DATE + ", "
					+ COLUMN_NAME_START_TIME + ", " + COLUMN_NAME_END_TIME 
					+ " FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_YEAR_MONTH + " = '" + year + month + "'";

		// Open database for Read / Write
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Workday workday = new Workday();
				workday.setId(Long.valueOf(cursor.getLong(0)));
				workday.setDay(Date.valueOf(cursor.getString(1)));
				if (cursor.getString(2) != null) {
					workday.setStartTime(Time.valueOf(cursor.getString(2)));
				}
				if (cursor.getString(3) != null) {
					workday.setEndTime(Time.valueOf(cursor.getString(3)));
				}

				// Adding to list
				workDaysList.add(workday);
			} while (cursor.moveToNext());
		}
		db.close();
		
		// return work days list
		return workDaysList;
	}
	
	/**
	 * Gets all the work days in a particular day
	 * @param year - in YYYY format
	 * @param month - in MM format 
	 * @return
	 */
	public static List<Workday> getWorkdayByDate(Date date) {
		List<Workday> workDaysList = new ArrayList<Workday>();

		// Select All Query
		String selectQuery = "SELECT " + COLUMN_NAME_ID + ", " + COLUMN_NAME_DATE + ", "
					+ COLUMN_NAME_START_TIME + ", " + COLUMN_NAME_END_TIME 
					+ " FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_DATE + " = '" + date.toString() + "'"
					+ " ORDER BY " + COLUMN_NAME_START_TIME + " ASC";

		// Open database for Read / Write
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Workday workday = new Workday();
				workday.setId(Long.valueOf(cursor.getLong(0)));
				workday.setDay(Date.valueOf(cursor.getString(1)));
				if (cursor.getString(2) != null) {
					workday.setStartTime(Time.valueOf(cursor.getString(2)));
				}
				if (cursor.getString(3) != null) {
					workday.setEndTime(Time.valueOf(cursor.getString(3)));
				}

				// Adding to list
				workDaysList.add(workday);
			} while (cursor.moveToNext());
		}
		db.close();
		
		// return work days list
		return workDaysList;
	}
	
	/**
	 * Gets the workday object corresponding to an id 
	 * @param id Long
	 * @return
	 */
	public static Workday getWorkdayById(Long id) {

		Workday workday = null;

		// Select Query
		String selectQuery = "SELECT "+COLUMN_NAME_DATE+", "+COLUMN_NAME_START_TIME+", "+COLUMN_NAME_END_TIME+" FROM "
				+ TABLE_NAME + " WHERE "+COLUMN_NAME_ID+" = '"+id.toString()+"'";

		// Open database for Read / Write
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			workday = new Workday();
			workday.setId(Long.valueOf(cursor.getLong(0)));
			workday.setDay(Date.valueOf(cursor.getString(1)));
			if (cursor.getString(2) != null) {
				workday.setStartTime(Time.valueOf(cursor.getString(2)));
			}
			if (cursor.getString(3) != null) {
				workday.setEndTime(Time.valueOf(cursor.getString(3)));
			}
		}
		db.close();
		
		// return workday 
		return workday;
	}
	
	/**
	 * Updates a work day in the database using the provided arguments
	 * If startTime is not null then startTime will be updated with the new value
	 * If endTime is not null then endTime will be updated with the new value
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static void updateWorkday(Long id, Time startTime, Time endTime) {

		// Update Query
		String updateClause;

		if (startTime != null && endTime != null) {
			updateClause = COLUMN_NAME_START_TIME + "='" + startTime.toString() + "', " 
							+ COLUMN_NAME_END_TIME + "='" + endTime.toString() + "'";
		} else if (startTime != null && endTime == null) {
			updateClause = COLUMN_NAME_START_TIME + "='" + startTime.toString() + "'"; 
		} else if (startTime == null && endTime != null) {
			updateClause = COLUMN_NAME_END_TIME + "='" + endTime.toString() + "'"; 
		} else {
			throw new IllegalArgumentException("startTime and endTime cannot be null at the same time!");
		}		

		String updateQuery = "UPDATE " + TABLE_NAME + " SET " + updateClause + " WHERE " 
								+ COLUMN_NAME_ID + " = '" + id.toString() + "'";

		// Open database for Read / Write
		final SQLiteDatabase db = open();
		db.execSQL(updateQuery);

	}	
	
	public static void deleteEntry(Long id) {
		String deleteQuery = "DELETE FROM " + TABLE_NAME + " WHERE " 
				+ COLUMN_NAME_ID + " = '" + id.toString() + "'";
		
		//Open database and remove the entry
		final SQLiteDatabase db = open();
		db.execSQL(deleteQuery);
	}
}