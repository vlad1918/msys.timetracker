package msys.timetracker;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.SharedPreferences;

public class Utils {

	public static final SimpleDateFormat HOUR_MIN_DATE_FORMAT       = new SimpleDateFormat("HH:mm", Locale.getDefault());
	public static final SimpleDateFormat MON_YEAR_DATE_FORMAT       = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
	public static final SimpleDateFormat DAY_NUMBER_DATE_FORMAT     = new SimpleDateFormat("dd", Locale.getDefault());
	public static final SimpleDateFormat DAY_SHORT_NAME_DATE_FORMAT = new SimpleDateFormat("EEE", Locale.getDefault());
	public static final String NO_TIME_AVAILABLE = "??:??";
	public static final String PREFERENCE_FILE = "msys.timetracker.preferences";
	public static final String PREFERENCE_WORKMODE = "msys.timetracker.preferences.workmode";
	private static final String[] _ROMANIAN_BANK_HOLIDAYS = {"01-01", "01-02", "01-24", "05-01", "08-15", "11-30", "12-01", "12-25", "12-26"};
	public static final List<String> ROMANIAN_BANK_HOLIDAYS = Arrays.asList(_ROMANIAN_BANK_HOLIDAYS);
	
	/**
	 * Returns the calculated work effort in a human readable format
	 * @param workdays
	 * @param workmode
	 * @return
	 */
	public static String calculateWorkEffort(List<Workday> workdays, Workmode workmode) {
		Map<Date, Long> work = new HashMap<Date, Long>();
		for (Workday workday : workdays) {
			if (workday.getEndTime() != null && workday.getStartTime() != null) {
				//initiate the work hash map
				if (! work.containsKey(workday.getDay())) {
					work.put(workday.getDay(), (workday.getEndTime().getTime() - workday.getStartTime().getTime()) / (60*1000));
				} else {
					Long temp = work.get(workday.getDay());
					work.put(workday.getDay(), temp + (workday.getEndTime().getTime() - workday.getStartTime().getTime()) / (60*1000));
				}
			}
		} 	

		//Calculate the difference for the total workdays
		int dif = 0;
		for (Entry<Date, Long> entry : work.entrySet()) {
			dif += Utils.calculateWorkDayMinutes(entry.getKey(), entry.getValue(), workmode);			
		}
		
		return Utils.convertMinutesToHM(dif);
	}
	
	/**
	 * Converts minutes in a string representation in the form HH:mm 
	 * @param minutes
	 * @return
	 */
	public static String convertMinutesToHM(int minutes) {
		boolean isNegative = false;
		
		if (minutes < 0) {
			minutes = - minutes;
			isNegative = true;
		}
		
		int hours = minutes / 60;
		int mins = minutes % 60;
		
		return  (isNegative ? "-" : "+") + String.format(Locale.GERMANY, "%02d:%02d", hours, mins);
	}
	
	/**
	 * Calculates the difference in minutes for a date.
	 * @param java.sql.Date 
	 * @param delta - the difference between start and end in minutes
	 * @param Workmode
	 * @return
	 */
	public static long calculateWorkDayMinutes(Date date, long delta, Workmode workmode) {
		
		long dif = 0; 
		int workNorm = 0;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date); 
		
        switch (workmode) {
		case PREGNANCY:
			if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY ) {
				workNorm = 4 * 60 + 30; //Work norm for Fridays is 4 hours and 30 minutes
			} else {
				workNorm = 7 * 60; //Work norm for MON - THU is 7 hours
			}			
			break;

		case PREGNANCY2:
			workNorm = 6 * 60 + 30; //Work norm is always 6 hours and 30 minutes			
			break;			
			
		case INTERNSHIP:
			workNorm = 4 * 60; //Work norm is always 4 hours			
			break;

		case INTERNSHIP2:
			if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY ) {
				workNorm = 3 * 60; //Work norm for Fridays is 3 hours
			} else {
				workNorm = 4 * 60 + 15; //Work norm for MON - THU is 4 hours and 15 minutes
			}			
			break;			
			
		case DEFAULT:
		default:			
			if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY ) {
				workNorm = 6 * 60 + 30; //Work norm for Fridays is 6 hours and 30 minutes
			} else {
				workNorm = 9 * 60; //Work norm for MON - THU is 9 hours
			}
			break;
		}
        		
		dif = delta - workNorm;		
		
		return dif;
	}
	
	/**
	 * Gets the work mode which is stored in a shared preferences file
	 * @param Context context
	 * @return Workmode enum
	 */
	public static Workmode getWorkmode(Context context) {
		SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
		String sWorkmode = sharedPref.getString(PREFERENCE_WORKMODE, Workmode.DEFAULT.name());
		return Workmode.valueOf(sWorkmode);
	}

	/**
	 * Sets the work mode which is stored in a shared preferences file
	 * @param Context context
	 * @param Workmode work mode to be set
	 * @return void
	 */
	public static void setWorkmode(Context context, Workmode workmode) {
		SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(PREFERENCE_WORKMODE, workmode.name());
		editor.commit();
	}
}
